from __future__ import unicode_literals

from django.db import models

# Create your models here.
import datetime
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class Page( models.Model ):
	header = models.CharField( max_length = 255 )
	text = models.TextField( max_length = 8191 )

	image = models.ImageField( 'Image', upload_to = 'images/', blank=True, null=True )
	change_date = models.DateTimeField( 'last change date' )

	def __str__( self ):
		return self.header

@python_2_unicode_compatible
class Locale( models.Model ):
	head = models.CharField( max_length = 255, primary_key = True )
	text = models.CharField( max_length = 255 )

	def __str__( self ):
		return self.head