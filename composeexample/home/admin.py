from django.contrib import admin
from .models import Page, Locale

# Register your models here.
admin.site.register(Locale)
admin.site.register(Page)
