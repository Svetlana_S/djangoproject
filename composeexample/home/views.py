from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone

from .models import Page, Locale

def index( request ):
	page_list = Page.objects.order_by(
		'-change_date' )

	locale = Locale.objects

	context = {
		'page_list' : page_list,
		'locale' : locale,
	}

	return render(request, 'index.html', context)

def show_page( request, page_id ):
	page = get_object_or_404(
		Page,
		pk = page_id )

	page_list = Page.objects.order_by(
		'-change_date' )

	locale = Locale.objects

	context = {
		'page' : page,
		'page_list' : page_list,
		'locale' : locale,
		'title' : page.header,
	}

	return render(
		request,
		'page.html',
		context )

